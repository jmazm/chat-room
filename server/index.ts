import Koa from 'koa';
import http from 'http';
import socketIo from 'socket.io';

const app = new Koa();

const server = http.createServer(app.callback());

const io = socketIo(server);

io.on('connection', socket => {
  socket.emit('request'); // emit an event to the socket
  io.emit('broadcast'); // emit an event to all connected sockets
  socket.on('chat', (msg) => {
    console.log(msg);
  }); // listen to the event
});


server.listen(3000);