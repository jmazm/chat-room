import Router from 'koa-router';
import path from 'path';
import fs from 'fs';
const router = new Router();

router.get('/', async (ctx) => {
  const data = await fs.readFileSync(path.resolve(process.cwd(), './client/index.html'), 'utf8');

  ctx.body = data;
});

export default router;