import React from 'react';
import ReactDOM from 'react-dom';
import App from './routes';
import './styles/reset.scss';


ReactDOM.render(
  <App />,
  document.getElementById('root')
);

