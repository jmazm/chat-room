import React from 'react';
import { HashRouter as Router, Route } from 'react-router-dom';
import Login from '../pages/Login';
import Chatting from '../pages/ChattingRoom';


export default function App () {
    return (
       <Router>
            <div>
                <Route exact={true} path="/" component={ Login }/>
                <Route path="/chatting" component={ Chatting }/>
            </div>
       </Router>
    );
}