// import * as React from 'react';
import React, { Component } from 'react';
import '../styles/chattingRoom.scss';
import { emitChat } from '../apis/chat';
import ChattingInputBoard from '../components/ChattingInputBoard';

export default class ChattingRoom extends Component {
  constructor(props: any) {
    super(props);
    this.state = {
    };

  }
  render() {
    return (
      <div className="chatting-room-wrapper">
        <div className="chatting-member-area">11</div>
        <div className="chatting-area">
          <div className="chatting-info-board">
            11
          </div>
          <ChattingInputBoard/>
        </div>
      </div>
    );
  }
  componentDidMount() {
    emitChat();
  }
}