// import * as React from 'react';
import React, { Component } from 'react';
import '../styles/login.scss';


export default class ChattingRoom extends Component {
  constructor(props: any) {
    super(props);
    this.state = {
    };

  }
  render() {
    return (
      <div className="chatting-room-login-wrapper">
            <h2 className="title">用户登录</h2>
            <div className="operation">
                <input placeholder="请输入您的昵称" className="input input-name"/>
                <button className="btn js-enter">进入聊天室</button>
            </div>
      </div>
    );
  }
  componentDidMount() {
  }
}